package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;
import java.util.Comparator;

@RestController
public class HelloController {

    @Autowired
    private ApplicationContext context;

    @Autowired
    private Environment environment;

    @RequestMapping("/")
    public String index() {
        return "Greetings from " + context.getApplicationName();
    }

}
