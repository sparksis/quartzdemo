package hello.quartz;

import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Random;

import static org.quartz.SimpleScheduleBuilder.repeatSecondlyForever;
import static org.quartz.TriggerKey.triggerKey;

@Configuration
public class MyJobConfiguration implements InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyJobConfiguration.class);

    private static final String JOB_KEY = MyJob.class.getSimpleName();
    private static final String TRIGGER_KEY = JOB_KEY + "_Trigger";

    @Autowired
    private Scheduler scheduler;

    @Autowired
    @Value("${myJob.interval}")
    private int interval;

    @Bean
    public JobDetail jobDetail() {
        return JobBuilder.newJob(MyJob.class)
                .withIdentity(JOB_KEY)
                .withDescription("Performing some 5 second action")
                .storeDurably()
                .build();
    }

    @Bean("random")
    public Random random() {
        return new Random(Integer.MAX_VALUE);
    }

    @Override
    public void afterPropertiesSet() {
        Trigger newTrigger = TriggerBuilder.newTrigger()
                .withIdentity(triggerKey(TRIGGER_KEY))
                .forJob(JOB_KEY)
                .withSchedule(repeatSecondlyForever(interval))
                .build();

        try {
            if (scheduler.checkExists(triggerKey(TRIGGER_KEY))) {
                scheduler.rescheduleJob(triggerKey(TRIGGER_KEY), newTrigger);
            } else {
                scheduler.scheduleJob(newTrigger);
            }
        } catch (SchedulerException e) {
            LOGGER.error("Unable to unschedule jobs", e);
        }

    }
}
