package hello.quartz;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.Date;
import java.util.Random;

@DisallowConcurrentExecution
public class MyJob implements Job {

    @Autowired
    private Random random;

    @Value("${spring.application.name}")
    private String name;

    @Override
    public void execute(JobExecutionContext context) {
        System.out.printf("Starting job {%s} @ %s\n", context.getJobDetail().getKey().getName(), new Date());
        String message = name != null ? "succeeded" : "failed";
        System.out.printf("Injection %s; with name: %s\n", message, name);
        try {
            Thread.sleep(random.nextInt(30) * 1000);
        } catch (InterruptedException e) {
            throw new RuntimeException("Hiccup", e);
        }
        System.out.printf("Finishing job @ %s\n", new Date());
    }
}
