package hello.quartz;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.quartz.SchedulerFactoryBeanCustomizer;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;


@Configuration
public class QuartzConfiguration implements SchedulerFactoryBeanCustomizer {

    private static final Logger LOGGER = LoggerFactory.getLogger(QuartzConfiguration.class);

    @Override
    public void customize(SchedulerFactoryBean schedulerFactoryBean) {
        // Any additional configuration not handled by the "spring.quartz" properties.
    }

}
